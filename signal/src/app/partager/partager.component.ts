import { Component } from '@angular/core';

@Component({
  selector: 'app-partager',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div class="share">
        <div class="shareTxt">
          <h2>Partager sans insécurité</h2>
          <p>
            Un chiffrement de bout en bout de pointe (propulsé par le protocole
            Signal à code source ouvert) assure la sécurité de vos
            conversations. Nous ne pouvons ni lire vos messages ni écouter vos
            appels, et personne d’autre que vous ne peut le faire. La
            confidentialité n’est pas proposée en option, c’est simplement la
            façon dont Signal fonctionne. Pour tous les messages, pour tous les
            appels, tout le temps.
          </p>
        </div>
        <div>
          <img class="shareImage" src="/assets/animation.png" alt="" />
        </div>
      </div>
    </section>
  `,
  styleUrl: './partager.component.css',
})
export class PartagerComponent {}
