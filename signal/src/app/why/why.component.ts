import { Component } from '@angular/core';

@Component({
  selector: 'app-why',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div class='why'>
        <h2>Pourquoi utiliser Signal ?</h2>
        <p>Explorez ci-dessous afin de découvrir pourquoi Signal est une messagerie simple, puissante et sécurisée</p>
      </div>
    </section>
  `,
  styleUrl: './why.component.css',
})
export class WhyComponent {}
