import { Component } from '@angular/core';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [NgbAlert],
  template: `
    <nav style="z-index:5" class="flex w100">
      <div class="flex w100 navCont">
        <div>
          <img class="logo-med" src="/assets/logo.png" alt="Logo signal" />
        </div>
        <div class="flex ">
          <a class="menuLink" href="">Get Signal</a>
          <a class="menuLink" href="">Help</a>
          <a class="menuLink" href="">Blog</a>
          <a class="menuLink" href="">Developers</a>
          <a class="menuLink" href="">careers</a>
          <a class="menuLink" href="">Donate</a>
          <a href=""
            ><img
              class="petit-logo"
              src="/assets/twitter.svg"
              alt="Logo twitter"
          /></a>
          <a href=""
            ><img
              class="petit-logo"
              src="/assets/instagram.svg"
              alt="Logo instagram"
          /></a>
          <a
            href="#"
            class="flex"
            role="button"
            aria-haspopup="true"
            aria-expanded="false"
            aria-label="Change Language"
            id="language-selector"
          >
            <img
              class="petit-logo mr-4"
              src="/assets/solid_globe.svg"
              alt=""
              srcset="logo de languse"
            />
            <div>Français</div>
          </a>
        </div>
      </div>
    </nav>
  `,
  styleUrl: './menu.component.css',
})
export class MenuComponent {}
