import { Component } from '@angular/core';

@Component({
  selector: 'app-avantages-m',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div class="topAvantM">
        <div class="infoTop">
          <h2>Aucune publicité, aucun traqueur, vraiment.</h2>
          <p>
            Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni
            aucun système de suivi inquiétant. Vous pouvez vous consacrer à
            partager les moments importants avec les personnes qui comptent pour
            vous.
          </p>
        </div>
        <div><img class="Illustration" src="/assets/No-Ads.png" alt="" /></div>
      </div>
      <div class="botAvantM">
        <img class="Illustration" src="/assets/Nonprofit503.png" alt="" />
        <div class="infoBot">
          <h2>Gratuit pour tous</h2>
          <p>
            Signal est un organisme à but non lucratif indépendant. Nous ne
            sommes reliés à aucune entreprise technologique importante et nous
            ne pourrons jamais être achetés par l’une d’elles. Le développement
            de notre plateforme est financé par des subventions et des dons de
            personnes comme vous.
          </p>
          <a href="">Faire un don a signal</a>
        </div>
      </div>
    </section>
  `,
  styleUrl: './avantages-m.component.css',
})
export class AvantagesMComponent {}
