import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvantagesMComponent } from './avantages-m.component';

describe('AvantagesMComponent', () => {
  let component: AvantagesMComponent;
  let fixture: ComponentFixture<AvantagesMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AvantagesMComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AvantagesMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
