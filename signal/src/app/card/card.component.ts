import { Component, Input } from '@angular/core';
import { Avantage } from '../type';
import { AvantagesComponent } from '../avantages/avantages.component';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-card',
  standalone: true,
  imports: [AvantagesComponent,CommonModule],
  template: `
      <div class="cardy div{{avantage.id}}">
        <img class="cardImage" src={{avantage.img}} alt="" srcset="" />
        <h3 class="cardTitle">{{avantage.h2}}</h3>
        <p class="cardInfos">
         {{avantage.p}}
        </p>
      </div>

  `,
  styleUrl: './../avantages/avantages.component.css',
})
export class CardComponent {
@Input() avantage!:Avantage
}
