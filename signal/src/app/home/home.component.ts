import { Component } from '@angular/core';
import { AvantagesComponent } from "../avantages/avantages.component";
import { MenuComponent } from "../menu/menu.component";
import { ExprimezComponent } from "../exprimez/exprimez.component";
import { WhyComponent } from "../why/why.component";
import { PartagerComponent } from "../partager/partager.component";
import { FooterComponent } from "../footer/footer.component";
import { AvantagesMComponent } from "../avantages-m/avantages-m.component";
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-home',
    standalone: true,
    template: `
    <section style="height: 470vh">

      <app-menu></app-menu>
      <app-exprimez class="exprimezMain"></app-exprimez>
      <app-why></app-why>
      <app-partager></app-partager>
      <app-avantages></app-avantages>
      <app-avantages-m></app-avantages-m>
      <app-footer></app-footer>
    </section>
  `,
    styleUrl: './home.component.css',
    imports: [NgbPaginationModule, NgbAlertModule ,AvantagesComponent, MenuComponent, ExprimezComponent, WhyComponent, PartagerComponent, FooterComponent, AvantagesMComponent]
})
export class HomeComponent {

}
