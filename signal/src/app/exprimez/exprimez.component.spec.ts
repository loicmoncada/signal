import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExprimezComponent } from './exprimez.component';

describe('ExprimezComponent', () => {
  let component: ExprimezComponent;
  let fixture: ComponentFixture<ExprimezComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ExprimezComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ExprimezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
