import { Component } from '@angular/core';

@Component({
  selector: 'app-exprimez',
  standalone: true,
  imports: [],
  template: `
    <section id='expBg'>
      <div class="exp">
        <div class="expLeft">
          <h1>Exprimez-vous librement</h1>
          <p>
            Dites « Bonjour » à une expérience de messagerie différente. Un
            accent inattendu sur la confidentialité et la protection des données
            personnelles, combiné à toutes les fonctions auxquelles vous vous
            attendez.
          </p>
          <a class="btnObtenir" href="">Obtenir Signal</a>

        </div>
        <div class="expRight">
          <img id="phone1" src="/assets/signal-iphone.png" alt="illustration telephone 1" />
          <img id="phone2" src="/assets/signal-pixel.png" alt="illustration telephone 2" />
        </div>
      </div>
    </section>
  `,
  styleUrl: './exprimez.component.css',
})
export class ExprimezComponent {}
