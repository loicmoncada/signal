import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div class="footerContent">
        <div class="propos">
          <div>
            © 2013–2024 Signal, a 501c3 nonprofit. Signal is a registered
            trademark in the United States and other countries.
          </div>
          <div>
            <span>For media inquiries, contact <a href="">press&#64;signal.org</a></span>
          </div>
          
        </div>
        <div>
          <strong>Organization</strong>
          <ul>
            <li>
              <a href=""> Donate </a>
            </li>
            <li>
              <a href=""> Career </a>
            </li>
            <li>
              <a href=""> Blog </a>
            </li>
            <li>
              <a href=""> Terms & Privacy Policy </a>
            </li>
          </ul>
        </div>
        <div>
          <strong>Download</strong>
          <ul>
            <li>
              <a href=""> Android </a>
            </li>
            <li>
              <a href=""> iPhone & iPad </a>
            </li>
            <li>
              <a href=""> Windows </a>
            </li>
            <li>
              <a href=""> Mac </a>
            </li>
            <li>
              <a href=""> Linux </a>
            </li>
          </ul>
        </div>
        <div>
          <strong>Social</strong>
          <ul>
            <li>
              <a href=""> GitHub </a>
            </li>
            <li>
              <a href=""> Twitter </a>
            </li>
            <li>
              <a href=""> Instagram </a>
            </li>
          </ul>
        </div>
        <div>
          <strong>Help</strong>
          <ul>
            <li>
              <a href=""> Support Center </a>
            </li>
            <li>
              <a href=""> Community </a>
            </li>
          </ul>
        </div>
      </div>
    </section>
  `,
  styleUrl: './footer.component.css',
})
export class FooterComponent {}
