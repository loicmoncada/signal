import { Component } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { CommonModule } from '@angular/common';
import { Avantage } from '../type';

@Component({
  selector: 'app-avantages',
  standalone: true,
  template: `
    <section>
      <div class="cardGrid">
        <app-card
          class="avantageContainer"
          *ngFor="let avantage of avantageList"
          [avantage]="avantage"
        ></app-card>

      </div>
    </section>
  `,
  styleUrl: './avantages.component.css',
  imports: [CardComponent, CommonModule],
})
export class AvantagesComponent {
  avantageList: Array<Avantage> = [
    {
      id:1,
      img: '/assets/Media.png',
      h2: 'Dites ce qui vous plaît',
      p: 'Partagez gratuitement des textes, des messages vocaux, des photos, des vidéos et des GIF. Signal utilise la connexion de données de votre téléphone afin de vous éviter les frais relatifs à l’envoi de texto et de message multimédia.',
    },
    {
      id:2,
      img: '/assets/Calls.png',
      h2: 'Exprimez-vous librement',
      p: 'Passez des appels vocaux ou vidéo clairs avec des personnes qui viventà l’autre bout de la ville ou du monde, sans frais supplémentaires',
    },
    {
      id:3,
      img: '/assets/Stickers.png',
      h2: 'Respectez votre vie privée',
      p: 'Avec nos stickers chiffrés, exprimez-vous encore plus librement. Vous êtes même libre de créer et de partager vos propres packs de stickers.',
    },
    {
      id:4,
      img: '/assets/Groups.png',
      h2: 'Rassemblez-vous avec les groupes',
      p: 'Avec les conversations de groupe, restez facilement en contact avec votre famille, vos amis et vos collègues.',
    },
  ];
}
